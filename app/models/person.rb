class Person < ActiveRecord::Base
  has_many :invitations
  has_many :meetings, :through => :invitations
  has_and_belongs_to_many :tasks
end
