class Task < ActiveRecord::Base
  belongs_to :point
  has_and_belongs_to_many :people
end
