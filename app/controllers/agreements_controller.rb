class AgreementsController < ApplicationController
  before_action :find_agreement, only: [:show, :update, :destroy]
 
  def index
    
    @agreement = Agreement.all
    render json: @agreement

  end

  def create
    @agreement = Agreement.new(agreement_params)
    if @agreement.save
      render json: @agreement, status: :created, location: @agreement
    else
      render json: @agreement.errors, status: :unproccessable_entity
    end
  end

  def show

    render json: @agreement

  end

  def update
    if @agreement.update(agreement_params)
      render json: @agreement, status: :accepted, location: @agreement
    else
      render json: @agreement.errors, status: :unprocessable_entity
    end
  end

  def destroy
    render json: @agreement, status: :ok if @agreement.destroy
  end

  private

  def find_agreement
    @agreement = Agreement.find(params[:id])
  end

  def agreement_params
    params.require(:agreement)
        .permit(:point_id, :agreement)
  end
end
