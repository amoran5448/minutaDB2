class PeopleController < ApplicationController
  before_action :find_person, only: [:show, :update]

  def index

    person = Person.all
    render json: person

  end

  def create
    @person = Person.new(person_params)
    if @person.save
      render json: @person, status: :created, location: @person
    else
      render json: @person.errors, status: :unproccessable_entity
    end
  end

  def show

    render json: @person

  end

  def update
    if @person.update(person_params)
      render json: @person, status: :accepted, location: @person
    else
      render json: @person.errors, status: :unprocessable_entity
    end
  end

  private

  def find_person
    @person = Person.find(params[:id])
  end

  def person_params
    params.require(:person)
        .permit(:name, :last_name, :phone_number, :email)
  end

end
