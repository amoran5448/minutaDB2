class PointsController < ApplicationController
  before_action :find_point, only: [:show, :update, :destroy, :tasks]
 
  def index
    
    @point = Point.all
    render json: @point

  end

  def create
    @point = Point.new(point_params)
    if @point.save
      render json: @point, status: :created, location: @point
    else
      render json: @point.errors, status: :unproccessable_entity
    end
  end

  def show

    render json: @point

  end

  def update
    if @point.update(point_params)
      render json: @point, status: :accepted, location: @point
    else
      render json: @point.errors, status: :unprocessable_entity
    end
  end

  def destroy
    render json: @point, status: :ok if @point.destroy
  end

  def tasks

    @points_tasks = @point.task
    render json: @points_tasks

  end

  private

  def find_point
    @point = Point.find(params[:id])
  end

  def point_params
    params.require(:point)
        .permit(:meeting_id, :point)
  end

end
