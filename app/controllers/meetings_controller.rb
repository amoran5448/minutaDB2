class MeetingsController < ApplicationController
  before_action :find_meeting, only: [:show, :update, :people]

#main
  def index_main
    
  text2 = {'title' => "Bonjour et bienvenue"}
  render html: text2['title']

  end
#--------------
  def index
    
    @meeting = Meeting.all
    render json: @meeting

  end

  def create
    @meeting = Meeting.new(params[:theme][:place][:date_appointment][:hour_appointment])
    if @meeting.save
      render json: @meeting, status: :created, location: @meeting
    else
      render json: @meeting.errors, status: :unproccessable_entity
    end
  end

  def show

    render json: @meeting, status: :ok

  end

  def update
    if @meeting.update(meeting_params)
      render json: @meeting, status: :accepted, location: @meeting
    else
      render json: @meeting.errors, status: :unprocessable_entity
    end
  end

  def people

    @meet_people = @meeting.people
    render json: @meet_people

  end

  private

  def meeting_params
    params.require(:meeting) 
        .permit(:theme, :place)
  end

  def find_meeting
    @meeting = Meeting.find(params[:id])
  end

end
