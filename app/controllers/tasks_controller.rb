class TasksController < ApplicationController
  before_action :find_task, only: [:show, :update, :destroy]

  def index
    
    @tasks = Task.all
    render json: @tasks

  end

  def create
    @tasks = Task.new(task_params)
    if @tasks.save
      render json: @tasks, status: :created, location: @tasks
    else
      render json: @tasks.errors, status: :unproccessable_entity
    end
  end

  def show

    @tasks = Task.find(params[:id])
    render json: @tasks

  end

  def update
    if @task.update(task_params)
      render json: @task, status: :accepted, location: @task
    else
      render json: @task.errors, status: :unprocessable_entity
    end
  end

  def destroy
    render json: @task, status: :ok if @task.destroy
  end

end

  private

  def find_task
    @task = Task.find(params[:id])
  end

  def task_params
    params.require(:task)
        .permit(:point_id, :task, :expiration_date, :status)
  end
