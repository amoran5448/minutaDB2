# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160517163551) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "agreements", force: :cascade do |t|
    t.integer "point_id",  null: false
    t.text    "agreement"
  end

  create_table "invitations", force: :cascade do |t|
    t.integer "person_id",  null: false
    t.integer "meeting_id", null: false
    t.string  "status"
    t.boolean "confirm"
  end

  create_table "meetings", force: :cascade do |t|
    t.string "theme",            limit: 100, null: false
    t.string "place",            limit: 255, null: false
    t.date   "date_appointment",             null: false
    t.time   "hour_appointment",             null: false
  end

  create_table "people", force: :cascade do |t|
    t.string "name",         limit: 100, null: false
    t.string "last_name",    limit: 100, null: false
    t.string "phone_number", limit: 12,  null: false
    t.string "email",        limit: 100, null: false
  end

  create_table "points", force: :cascade do |t|
    t.integer "meeting_id",             null: false
    t.string  "point",      limit: 255
  end

  create_table "tasks", force: :cascade do |t|
    t.integer "point_id",                    null: false
    t.string  "task",            limit: 255, null: false
    t.date    "expiration_date",             null: false
    t.boolean "status",                      null: false
  end

  create_table "tasks_manager", force: :cascade do |t|
    t.integer "person_id", null: false
    t.integer "task_id",   null: false
  end

  add_foreign_key "agreements", "points", name: "agreements_point_id_fkey"
  add_foreign_key "invitations", "meetings", name: "invitations_meeting_id_fkey"
  add_foreign_key "invitations", "people", name: "invitations_person_id_fkey"
  add_foreign_key "points", "meetings", name: "points_meeting_id_fkey"
  add_foreign_key "tasks", "points", name: "tasks_point_id_fkey"
  add_foreign_key "tasks_manager", "people", name: "task_manager_person_id_fkey"
  add_foreign_key "tasks_manager", "tasks", name: "task_manager_task_id_fkey"
end
