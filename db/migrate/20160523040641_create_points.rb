class CreatePoints < ActiveRecord::Migration
  def change
    create_table :points do |t|
      t.references :meeting, index: true, foreign_key: true
      t.string :point

      t.timestamps null: false
    end
  end
end
