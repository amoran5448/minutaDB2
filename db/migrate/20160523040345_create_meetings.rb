class CreateMeetings < ActiveRecord::Migration
  def change
    create_table :meetings do |t|
      t.string :theme
      t.string :place
      t.date :date_appointment
      t.time :hour_appointment

      t.timestamps null: false
    end
  end
end
