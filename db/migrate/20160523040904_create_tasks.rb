class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.references :point, index: true, foreign_key: true
      t.string :task
      t.date :expiration_date
      t.boolean :status

      t.timestamps null: false
    end
  end
end
