class CreateAgreements < ActiveRecord::Migration
  def change
    create_table :agreements do |t|
      t.references :point, index: true, foreign_key: true
      t.string :agreement

      t.timestamps null: false
    end
  end
end
